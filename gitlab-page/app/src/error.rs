use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Env {0}")]
    Env(std::env::VarError),

    #[error("Request {0}")]
    Request(tracker_api_reqwest::error::Error),

    #[error("Invalid {0}")]
    Invalid(String),

    #[error("NotImplment {0}")]
    NotImplment(String),
}
