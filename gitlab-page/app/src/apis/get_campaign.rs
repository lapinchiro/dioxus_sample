use tracker_api_reqwest::{Method::Get, Response};

pub async fn send(
    url: String,
    x_pass_code: String,
    request_timeout_sec: Option<u64>,
    connect_timeout_sec: Option<u64>,
) -> Result<Response, crate::error::Error> {
    tracker_api_reqwest::execute(
        Get,
        url,
        x_pass_code,
        None,
        request_timeout_sec,
        connect_timeout_sec,
    )
    .await
    .map_err(|e| crate::error::Error::Request(e))
}
