use dioxus::prelude::*;

#[derive(Props, PartialEq)]
pub struct MyProps<'a> {
    name: &'a str,
}

#[allow(non_snake_case)]
pub fn MyComponent<'a>(cx: Scope<'a, MyProps<'a>>) -> Element {
    cx.render(rsx! {
        div {
            "sub"
        }
        div {
            "my_component: {cx.props.name}"
        }
    })
}
