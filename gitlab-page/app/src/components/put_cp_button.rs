use crate::apis::put_campaign;
use chrono::prelude::Utc;
use dioxus::prelude::*;
use tracker_api_reqwest::params::PutRequestParams;

const URL: &str =
    "https://tgujngalvnteuurw4us2vyo4jq0hsyvf.lambda-url.us-east-1.on.aws/api/v1/service/campaigns";
const WEBHOOK_URL: &str = "https://example.com";
const X_PASS_CODE: &str = "x_pass_code";

#[derive(Props, PartialEq)]
pub struct MyProps<'a> {
    name: &'a str,
}

#[allow(non_snake_case)]
pub fn PutCpButton<'a>(cx: Scope<'a, MyProps<'a>>) -> Element {
    let _name = use_state(&cx, || "test");

    let future = use_future(cx, (), |_| async { request("name").await });

    cx.render(match future.value() {
        Some(Ok(response)) => rsx! {
            button {
                onclick: move |_| future.restart(),
                "Put Campaign!"
            }
            div {
                "{response}"
            }
        },
        Some(Err(_)) => rsx! { div { "Loading dogs failed" } },
        None => rsx! { div { "Loading dogs..." } },
    })
}

async fn request(name: &str) -> Result<String, crate::error::Error> {
    let params = build_put_request_params(name);

    let res = put_campaign::send(
        format!("{url}/{cp_name}", url = URL, cp_name = name),
        X_PASS_CODE.to_owned(),
        params,
        None,
        None,
    )
    .await;

    Ok(match res {
        Ok(res) => res.text().await.map_err(|e| {
            crate::error::Error::Request(tracker_api_reqwest::error::Error::Reqwest(e))
        })?,
        Err(err) => err.to_string(),
    })
}

fn build_put_request_params(name: &str) -> PutRequestParams {
    let now = Utc::now().timestamp_millis();
    PutRequestParams::new(
        name.to_owned(),
        now - 1 * 1 * 1 * 60 * 1000,
        now + 1 * 24 * 60 * 60 * 1000,
        "dioxus".to_owned(),
        WEBHOOK_URL.to_owned(),
        X_PASS_CODE.to_owned(),
        None,
    )
}
