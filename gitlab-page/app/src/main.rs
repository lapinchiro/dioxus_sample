use dioxus::prelude::*;
mod components;
mod apis;
mod models;
mod forms;
mod error;


fn main() {
    dioxus_web::launch(app);
}

fn app(cx: Scope) -> Element {
    let mut count = use_state(&cx, || 0);
    cx.render(rsx! {
        div {}, "count: { count }"
        button { onclick: move |_| count += 1, "++" }
        components::first::MyComponent {
            name: "hogehoge"
        }
        components::put_cp_button::PutCpButton {
            name: "text"
        }
    })
}
