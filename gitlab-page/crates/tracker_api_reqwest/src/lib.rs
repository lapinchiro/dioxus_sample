mod client;
pub mod error;
pub mod params;
pub use reqwest::Response;

pub enum Method {
    Get,
    Put,
    Delete,
}

pub async fn execute(
    method: Method,
    url: String,
    x_pass_code: String,
    params: Option<params::PutRequestParams>,
    request_timeout_sec: Option<u64>,
    connect_timeout_sec: Option<u64>,
) -> Result<Response, error::Error> {
    let response = match method {
        Method::Get => get(url, x_pass_code, request_timeout_sec, connect_timeout_sec).await?,
        Method::Put => match params {
            Some(params) => {
                put(
                    url,
                    params,
                    x_pass_code,
                    request_timeout_sec,
                    connect_timeout_sec,
                )
                .await
            }
            None => Err(error::Error::Invalid("params is required".to_owned())),
        }?,

        Method::Delete => {
            delete(url, x_pass_code, request_timeout_sec, connect_timeout_sec).await?
        }
    };

    Ok(response)
}

async fn get(
    url: String,
    x_pass_code: String,
    request_timeout_sec: Option<u64>,
    connect_timeout_sec: Option<u64>,
) -> Result<reqwest::Response, error::Error> {
    make_client(x_pass_code, request_timeout_sec, connect_timeout_sec)?
        .get(url)
        .await
}

async fn put(
    url: String,
    params: params::PutRequestParams,
    x_pass_code: String,
    request_timeout_sec: Option<u64>,
    connect_timeout_sec: Option<u64>,
) -> Result<reqwest::Response, error::Error> {
    make_client(x_pass_code, request_timeout_sec, connect_timeout_sec)?
        .put(url, &serde_json::to_value(params)?)
        .await
}

async fn delete(
    url: String,
    x_pass_code: String,
    request_timeout_sec: Option<u64>,
    connect_timeout_sec: Option<u64>,
) -> Result<reqwest::Response, error::Error> {
    make_client(x_pass_code, request_timeout_sec, connect_timeout_sec)?
        .delete(url)
        .await
}

fn make_client(
    x_pass_code: String,
    request_timeout_sec: Option<u64>,
    connect_timeout_sec: Option<u64>,
) -> Result<client::ApiClient, error::Error> {
    client::ApiClient::new(
        request_timeout_sec.unwrap_or(30),
        connect_timeout_sec.unwrap_or(10),
        x_pass_code,
    )
}
