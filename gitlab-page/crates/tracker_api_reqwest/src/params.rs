use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Serialize, Deserialize)]
pub struct PutRequestParams {
    campaign_code: String,
    start_utsms: i64,
    end_utsms: i64,
    rule_code: String,
    endpoint_url: String,
    x_pass_code: String,
}

impl PutRequestParams {
    pub fn new(
        campaign_code: String,
        start_utsms: i64,
        end_utsms: i64,
        rule_code: String,
        endpoint_url: String,
        x_pass_code: String,
        campaign_id: Option<String>,
    ) -> Self {
        Self {
            campaign_code,
            start_utsms,
            end_utsms,
            rule_code,
            endpoint_url: Self::build_endpoint_url(
                &endpoint_url,
                &campaign_id.unwrap_or(Uuid::new_v4().to_string()),
            ),
            x_pass_code,
        }
    }

    fn build_endpoint_url(endpoint_url: &str, campaign_id: &str) -> String {
        if endpoint_url.contains('?') {
            format!(
                "{endpoint_url}&key={uuid}",
                endpoint_url = endpoint_url,
                uuid = campaign_id
            )
        } else {
            format!(
                "{endpoint_url}?key={uuid}",
                endpoint_url = endpoint_url,
                uuid = campaign_id
            )
        }
    }
}
