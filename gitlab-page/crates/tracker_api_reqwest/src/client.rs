use reqwest::{Client, ClientBuilder};
// use std::time::Duration;

pub struct ApiClient {
    pub client: Client,
    x_pass_code: String,
}

impl ApiClient {
    pub fn new(
        _request_timeout_sec: u64,
        _connect_timeout_sec: u64,
        x_pass_code: String,
    ) -> Result<Self, crate::error::Error> {
        let client = ClientBuilder::default()
            // targetをwasmでビルドしようとするとエラーになるので泣く泣くコメントアウトした。 
            // .timeout(Duration::from_secs(request_timeout_sec))
            // .connect_timeout(Duration::from_secs(connect_timeout_sec))
            .build()
            .map_err(crate::error::Error::Reqwest)?;

        Ok(Self {
            client,
            x_pass_code,
        })
    }

    pub async fn get(&self, url: String) -> Result<reqwest::Response, crate::error::Error> {
        self.client
            .get(url)
            .header("x_pass_code", self.x_pass_code.clone())
            .send()
            .await
            .map_err(|e| e.into())
    }

    pub async fn put(
        &self,
        url: String,
        params: &serde_json::Value,
    ) -> Result<reqwest::Response, crate::error::Error> {
        self.client
            .put(url)
            .json(params)
            .header("x_pass_code", self.x_pass_code.clone())
            .send()
            .await
            .map_err(|e| e.into())
    }

    pub async fn delete(&self, url: String) -> Result<reqwest::Response, crate::error::Error> {
        self.client
            .delete(url)
            .header("x_pass_code", self.x_pass_code.clone())
            .send()
            .await
            .map_err(|e| e.into())
    }
}
